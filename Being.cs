using System;
using System.Diagnostics;
using System.Drawing;

namespace zombie
{
    public class Being
    {
        private static readonly Random random = new Random();

        public enum BeingType
        {
            Zombie,
            Human,
            PanicHuman,
            Wall,
            None
        }

        int _active;

        public int Dir { get; set; }

        public static Being[] Beings { get; set; }

        public int XPos { get; private set; }

        public int XPosCenter => XPos + Settings.HalfBeingSize;

        public int YPos { get; private set; }

        public int YPosCenter => YPos + Settings.HalfBeingSize;

        public BeingType Type { get; private set; }

        public Being()
        {
            Dir = random.Next(4) + 1;
            Type = BeingType.Human;
            _active = 0;
        }

        public void Position(Bitmap bmp)
        {
            for (var ok = 0; ok < 100; ok++)
            {
                XPos = random.Next(Settings.CityWidth - 1) + (int)Settings.BeingSize;
                if (XPos >= Settings.CityWidth - (int)Settings.BeingSize)
                {
                    XPos = Settings.CityWidth - (int)Settings.BeingSize - 1;
                }

                YPos = random.Next(Settings.CityHeight - 1) + (int)Settings.BeingSize;
                if (YPos >= Settings.CityHeight - (int)Settings.BeingSize)
                {
                    YPos = Settings.CityHeight - (int)Settings.BeingSize - 1;
                }
#if ELLIPSE
                Debug.Assert(XPos > 0 && YPos > 0, "Invalid coordiantes.");
                Debug.Assert(XPos < Settings.CityWidth && YPos < Settings.CityHeight, "Invalid coordiantes.");
                Color testminx = bmp.GetPixel(XPos, YPos);
                Color testmaxy = bmp.GetPixel(XPos + (int)Settings.BeingSize, YPos + (int)Settings.BeingSize);
                if (testminx.ToArgb() == Settings.CityColor.ToArgb()
                    && testmaxy.ToArgb() == Settings.CityColor.ToArgb())
                {
                    break;
                }
#else
                Color test = bmp.GetPixel(XPos, YPos);
                if(test.ToArgb() == Settings.CityColor.ToArgb()) {
                    break;
                }
#endif
            }
        }

        public void Infect()
        {
            Type = BeingType.Zombie;
        }

        public void Infect(int x, int y)
        {
#if ELLIPSE
            if (XPosCenter + Settings.HalfBeingSize >= x
                &&
                XPosCenter - Settings.HalfBeingSize <= x
                &&
                YPosCenter + Settings.HalfBeingSize >= y
                &&
                YPosCenter - Settings.HalfBeingSize < y)
            {
                Type = BeingType.Zombie;
            }
#else
            if (x == XPos && y == YPos) {
                Type = BeingType.Zombie; 
            }
#endif
        }

        private BeingType Look(int x, int y, int d, int dist, Bitmap bmp)
        {
            for (var i = 0; i < dist; i++)
            {
                switch (d)
                {
                    case 1:
                        y -= (int)Settings.BeingSize;
                        break;
                    case 2:
                        x += (int)Settings.BeingSize;
                        break;
                    case 3:
                        y += (int)Settings.BeingSize;
                        break;
                    case 4:
                        x -= (int)Settings.BeingSize;
                        break;
                }

                if (x >= Settings.CityWidth - Settings.BeingSize
                    || x < Settings.BeingSize
                    || y >= Settings.CityHeight - Settings.BeingSize
                    || y < Settings.BeingSize
                )
                {
                    return BeingType.Wall;
                }

                //TODO: we still need to do some calcualtion so the paint of ellipsis doesn't destroy buildings
                //or maybe that's a feature or a crumbling zombie city?

                var pixl1 = bmp.GetPixel(x, y).ToArgb();
                if (pixl1 == Settings.BuildingColorArgb)
                {
                    return BeingType.Wall;
                }

                if (pixl1 == Settings.PanicHumanColorArgb)
                {
                    return BeingType.PanicHuman;
                }

                if (pixl1 == Settings.HumanColorArgb)
                {
                    return BeingType.Human;
                }

                if (pixl1 == Settings.ZombieColorArgb)
                {
                    return BeingType.Zombie;
                }
            }

            return BeingType.None;
        }

        public void Move(Bitmap bmp, Graphics g)
        {
            int r = random.Next(10);

            if (Type == BeingType.Human
                && (_active > 0 || r > Settings.PanicThreshold)
                || r == 1)
            {
                g.FillEllipse(Settings.CityColorBrush, XPos, YPos, Settings.BeingSize, Settings.BeingSize);

                BeingType look = Look(XPosCenter, YPosCenter, Dir, 1, bmp);
                if (look == BeingType.None)
                {
                    if (Dir == 1)
                    {
                        YPos--;
                    }

                    if (Dir == 2)
                    {
                        XPos++;
                    }

                    if (Dir == 3)
                    {
                        YPos++;
                    }

                    if (Dir == 4)
                    {
                        XPos--;
                    }
                }
                else if(look == BeingType.Wall)
                {
                    Dir = random.Next(4) + 1;
                }

                if (Type == BeingType.Zombie)
                {
                    g.FillEllipse(Settings.ZombieColorBrush, XPos, YPos, Settings.BeingSize, Settings.BeingSize);
                }
                else if (_active > 0)
                {
                    g.FillEllipse(Settings.PanicHumanColorBrush, XPos, YPos, Settings.BeingSize, Settings.BeingSize);
                }
                else
                {
                    g.FillEllipse(Settings.HumanColorBrush, XPos, YPos, Settings.BeingSize, Settings.BeingSize);
                }

                if (_active > 0)
                {
                    _active--;
                }
            }

            BeingType target = Look(XPosCenter, YPosCenter, Dir, 10, bmp);

            if (Type == BeingType.Zombie)
            {
                if (target == BeingType.Human || target == BeingType.PanicHuman)
                {
                    _active = 10;
                }

                if (_active == 0 && target != BeingType.Zombie)
                {
                    Dir = random.Next(4) + 1;
                }

                BeingType victim = Look(XPosCenter, YPosCenter, Dir, 1, bmp);

                if (victim == BeingType.Human || victim == BeingType.PanicHuman)
                {
                    int ix = XPos;
                    int iy = YPos;
                    if (Dir == 1)
                    {
                        iy -= (int)Settings.BeingSize;
                    }

                    if (Dir == 2)
                    {
                        ix += (int)Settings.BeingSize;
                    }

                    if (Dir == 3)
                    {
                        iy += (int)Settings.BeingSize;
                    }

                    if (Dir == 4)
                    {
                        ix -= (int)Settings.BeingSize;
                    }

                    for (int i = 0; i < Settings.NumberCitizens; i++)
                    {
                        Beings[i].Infect(ix, iy);
                    }
                }

                return;
            }

            if (Type == BeingType.Human)
            {
                if (target == BeingType.Zombie || target == BeingType.PanicHuman)
                {
                    if (target == BeingType.Zombie)
                    {
                        Dir = (Dir + 2)%4+1;
                    }
                    _active = 40;
                }

                if (random.Next(8) == 1)
                {
                    Dir = random.Next(4) + 1;
                }
            }
        }
    }
}