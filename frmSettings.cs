﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace zombie {
    public partial class frmSettings : Form {
        public frmSettings() {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e) {
            txtBuildings.Text = Settings.NumberOfBuildings.ToString();
            txtCitizens.Text = Settings.NumberCitizens.ToString();
            txtZombies.Text = Settings.NumberOfZombies.ToString();
        }

        private void btnOk_Click(object sender, EventArgs e) {
            Settings.NumberOfBuildings = Convert.ToInt32(txtBuildings.Text);
            Settings.NumberCitizens = Convert.ToInt32(txtCitizens.Text);
            Settings.NumberOfZombies = Convert.ToInt32(txtZombies.Text);
            Settings.SaveUserSettings();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
