using System.Drawing;
using System.Windows.Forms;

namespace zombie
{
    public static class Settings
    {
        private const string REG_NUM_BUILDINGS = "Num_Buildings";
        private const string REG_NUM_CITIZENS = "Num_Citizens";
        private const string REG_NUM_ZOMBIES = "Num_Zombies";

        private static int _humanColorArgb = -1;

        private static int _panicHumanColorArgb = -1;

        private static int _zombieColorArgb = -1;

        private static int _buildingColorArgb = -1;

        public static int NumberCitizens { get; set; }

        public static byte PanicThreshold { get; set; }

        public static byte MouseTolerance { get; set; }

        public static Color HumanColor { get; set; }

        public static int HumanColorArgb
        {
            get
            {
                if (_humanColorArgb < 0)
                {
                    _humanColorArgb = HumanColor.ToArgb();
                }

                return _humanColorArgb;
            }
        }

        public static Brush HumanColorBrush { get; set; }

        public static Color PanicHumanColor { get; set; }

        public static Brush PanicHumanColorBrush { get; set; }

        public static int PanicHumanColorArgb
        {
            get
            {
                if (_panicHumanColorArgb < 0)
                {
                    _panicHumanColorArgb = PanicHumanColor.ToArgb();
                }

                return _panicHumanColorArgb;
            }
        }

        public static Color ZombieColor { get; set; }

        public static int ZombieColorArgb
        {
            get
            {
                if (_zombieColorArgb < 0)
                {
                    _zombieColorArgb = ZombieColor.ToArgb();
                }

                return _zombieColorArgb;
            }
        }

        public static Brush ZombieColorBrush { get; set; }

        public static Color BuildingColor { get; set; }

        public static int BuildingColorArgb
        {
            get
            {
                if (_buildingColorArgb < 0)
                {
                    _buildingColorArgb = BuildingColor.ToArgb();
                }

                return _buildingColorArgb;
            }
        }

        public static Color CityColor { get; set; }

        public static Brush CityColorBrush { get; set; }

        public static int CityWidth { get; set; }

        public static int CityHeight { get; set; }

        public static int NumberOfBuildings { get; set; }

        public static float BeingSize { get; set; }

        public static int HalfBeingSize => (int)(BeingSize / 2);

        public static int NumberOfZombies { get; set; }

        internal static void SaveUserSettings()
        {
            //Rows/Cols
            if (Application.UserAppDataRegistry != null)
            {
                Application.UserAppDataRegistry.SetValue(REG_NUM_BUILDINGS, NumberOfBuildings);
                Application.UserAppDataRegistry.SetValue(REG_NUM_CITIZENS, NumberCitizens);
                Application.UserAppDataRegistry.SetValue(REG_NUM_ZOMBIES, NumberOfZombies);
                Application.UserAppDataRegistry.Flush();
            }
        }

        public static void ReadDefaultSettings()
        {
            BeingSize = 4.0f;

            if (Application.UserAppDataRegistry != null)
            {
                NumberCitizens = (int)Application.UserAppDataRegistry.GetValue(REG_NUM_CITIZENS, 1000);
                NumberOfBuildings = (int)Application.UserAppDataRegistry.GetValue(REG_NUM_BUILDINGS, 100);
                PanicThreshold = 4;
                MouseTolerance = 10;

                HumanColor = Color.White;
                HumanColorBrush = Brushes.White;

                PanicHumanColor = Color.Gold;
                PanicHumanColorBrush = Brushes.Gold;

                ZombieColor = Color.Red;
                ZombieColorBrush = Brushes.Red;

                BuildingColor = Color.Gray;

                CityColor = Color.Black;
                CityColorBrush = Brushes.Black;

                NumberOfZombies = (int)Application.UserAppDataRegistry.GetValue(REG_NUM_ZOMBIES, 1);
            }
        }
    }
}