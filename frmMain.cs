using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace zombie
{
    public class FrmMain : Form
    {
        private readonly Container components = null;
        private bool _exit;
        private int _mouseX;
        private int _mouseY;

        Graphics g;
        private Random random;

        private Bitmap ScreenBitmap;
        private Graphics ScreenGraphic;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void Setup()
        {
            Settings.ReadDefaultSettings();
            var minY = 0;
            var maxY = 0;
            var maxX = 0;
            var minX = 0;

#if DEBUG
            maxX = 800;
            maxY = 800;
#else
            foreach (Screen screen in Screen.AllScreens) {
                if (screen.Bounds.Left < minX)
                    minX = screen.Bounds.Left;

                if (screen.Bounds.Right > maxX)
                    maxX = screen.Bounds.Right;

                if (screen.Bounds.Bottom > maxY)
                    maxY = screen.Bounds.Bottom;

                if (screen.Bounds.Top < minY)
                    minY = screen.Bounds.Top;
            }
            this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            this.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
#endif
            Location = new Point(minX, minY);
            Height = maxY - minY;
            Width = maxX - minX;

            Settings.CityWidth = Width;
            Settings.CityHeight = Height;

            _mouseX = 0;
            _mouseY = 0;
            random = new Random();

            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            ScreenBitmap = new Bitmap(Settings.CityWidth, Settings.CityHeight);
            ScreenGraphic = Graphics.FromImage(ScreenBitmap);
            ScreenGraphic.SmoothingMode = SmoothingMode.AntiAlias;
            ScreenGraphic.Clear(Settings.CityColor);

            Brush wall = new SolidBrush(Settings.BuildingColor);
            Brush city = new SolidBrush(Settings.CityColor);
            var cityPen = new Pen(city) { Width = 2.0f };

            int x, y, w, h;
            for (var i = 0; i < Settings.NumberOfBuildings; i++)
            {
                x = random.Next(Width - 1) + 1;
                y = random.Next(Height - 1) + 1;
                w = random.Next(60) + 10;
                h = random.Next(60) + 10;
                ScreenGraphic.FillRectangle(wall,
                    x,
                    y,
                    w,
                    h);
                ScreenGraphic.DrawRectangle(cityPen, x, y, w, h);
            }

            for (var i = 0; i < Settings.NumberOfBuildings / 2; i++)
            {
                x = random.Next(Width - 1) + 1;
                y = random.Next(Height - 1) + 1;
                w = random.Next(20) + 20;
                h = random.Next(20) + 20;
                ScreenGraphic.FillRectangle(city,
                    x,
                    y,
                    w,
                    h);
            }

            Being.Beings = new Being[Settings.NumberCitizens];
            for (var i = 0; i < Settings.NumberCitizens; i++)
            {
                Being.Beings[i] = new Being();
                Being.Beings[i].Position(ScreenBitmap);
            }

            for (var i = 0; i < Settings.NumberOfZombies; i++)
            {
                Being.Beings[i].Infect();
            }

            g = Graphics.FromImage(ScreenBitmap);
        }

        private void Run()
        {
            Cursor.Hide();
            Show();
            Refresh();
            while (!_exit)
            {
                if (_exit)
                {
                    break;
                }

                Invalidate();
                Application.DoEvents();
            }

            Cursor.Show();
            Application.Exit();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                g.Dispose();
                if (components != null)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FrmMain
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.ControlBox = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMain";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Activated += new System.EventHandler(this.frmMain_Activated);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmMain_Paint);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseMove);
            this.ResumeLayout(false);

        }

        private void frmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseX == 0 && _mouseY == 0)
            {
                _mouseX = e.X;
                _mouseY = e.Y;
            }
            else
            {
                var distance = Math.Sqrt(Math.Pow(_mouseX - e.X, 2) + Math.Pow(_mouseY - e.Y, 2));
                if (distance >= Settings.MouseTolerance)
                {
                    _exit = true;
                }
            }
        }

        private void frmMain_Activated(object sender, EventArgs e)
        {
            Setup();
            Run();
        }

        private void frmMain_Paint(object sender, PaintEventArgs e)
        {
            RepositionBeings();
            e.Graphics.DrawImageUnscaled(ScreenBitmap, 0, 0);
        }

        private void RepositionBeings()
        {
            foreach (var b in Being.Beings)
            {
                b.Move(ScreenBitmap, g);
            }
        }
    }
}