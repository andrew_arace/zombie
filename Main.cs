﻿using System;
using System.Windows.Forms;

namespace zombie
{
    public class Startup
    {
        /// <summary>
        /// Application start point.
        /// /c is switch for config screen
        /// /s is start the screensaver
        /// /p is preview the screensaver in the little display window
        /// no switch runs the screensaver, same as /s
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            Settings.ReadDefaultSettings();
#if DEBUG
            Settings.SaveUserSettings();
#endif
            if (args.Length > 0)
            {
                if (String.Compare(args[0].Substring(0, 2), "/c", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    //display no options... 
                    Application.Run(new frmSettings());
                    return;
                }

                if (String.Compare(args[0], "/p", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Application.Exit();
                    return;
                }
            }

            try
            {
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FrmMain());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }
    }
}